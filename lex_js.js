let KEYWORDS = [
	'const', 'var', 'let',
	'case', 'break', 'continue',
	'class', 'catch', 'default',
	'do', 'while', 'else', 'if',
	'export', 'extends', 'finally',
	'for', 'function', 'in', 'new',
	'return', 'switch', 'this', 'throw',
	'try', 'typeof'
];

var chunk;
var tokens = [];

const lex = (input) => {
  let tokens = [], c, i = 0;


  	let isOperator = (c) => (/[+\-*\/\^%=]/.test(c)),
  	isSeparator = (c) => (/[(),;{}]/.test(c)),
  	isDigit = (c) => (/[0-9]/.test(c)),
  	isWhiteSpace = (c) => (/\s/.test(c)),
  	isNewLine = (c) => (/\n/.test(c)),
  	isCommentSlash = (c) => (/[\/]/.test(c)),
  	isExponential = (c) => (/[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)/.test(c)),
  	isString_l = (c) => (c == '"');

	let isIdentifier = (c) => ( 
		!isOperator(c) 
		&& !isWhiteSpace(c) 
		&& !isSeparator(c) 
	);

	let isKeyword = (c) => (
		KEYWORDS.indexOf(c) > -1
	);

  	let advance = () => (c = input[++i]);
  	let addToken = (type, value) => (
		tokens.push({
	  		type: type,
	  		value: value
  		})
	);

	while (i < input.length){
		c = input[i];

		if (isWhiteSpace(c)){
			advance();
		}
		else if(isCommentSlash(c)){
			let comm = c;
			let isdig = isDigit(advance());

			comm += c;
			if(isdig){
				addToken("OPERATOR", c);
			}else if(c == '*'){
				advance();
				let lastChar = c;
				let k = 0;

				while(!isCommentSlash(advance()) && lastChar != '*'){
					comm += lastChar;
					comm += c;
					lastChar = c;
					k++;
				}

				if(!k){	comm += lastChar;}
 				comm += c;
				addToken("COMMENT_MULTILINE", comm);
			}else if(c == '/'){
				while(advance() != "\n" && typeof c != 'undefined'){
					comm += c;
				}
				addToken("COMMENT", comm);
				advance();
			}
		}
		else if(isString_l(c)){
			let str = c;
			while(advance() != '"'){
				str += input[i];
			}
			str += '"';
			addToken("STRING", str);
			advance();
		}
		else if (isOperator(c)){
			addToken("OPERATOR", c);
			advance();
		}
		else if (isSeparator(c)){
			addToken("SEPARATOR", c);
			advance();
		}
		else if (isDigit(c)) {
		  	var num = c;
		  	while (isDigit(advance())){
				num += c;
			} 

			if (c === ".") {
				do{
				  num += c; 
				}while (isDigit(advance()));
			}else if(c.toLowerCase() === 'e'){
				num += c;				
				advance();
				if(c === '+' || c === '-'){
					do{						
				  		num += c; 				  		
					}while (isDigit(advance()));
				}
			}

			if(isExponential(num)){
				addToken("NUMBER_EXPONENTIAL", num);
			}else{
				num = parseFloat(num);
				if (!isFinite(num)){
					throw "Number is too large or too small for a 64-bit double.";  
				}
				addToken("NUMBER", num);
			}	
		}
		else if (isIdentifier(c)) {
			var idn = c;
			while (isIdentifier(advance())){
				idn += c;
			}
			if(isKeyword(idn)){
				addToken("KEYWORD", idn);
			}else{
				addToken("IDENTIFIER", idn);
			}
		}
		else throw "Unrecognized token " + c; 
	}

	addToken("(end)", "");
	return tokens;
}


function analyze(){
	let inputText = document.getElementById('inputText').value;
	let result = lex(inputText);
	let aaa = "<table>";

	for(let i=0; i < result.length; i++){
		aaa += "<tr><td>" + result[i].type + "</td><td><span class='res'>" + result[i].value + "</span></td></tr>";
	}

	aaa += "</table>"
	document.getElementById('resultBox').innerHTML = aaa;
}